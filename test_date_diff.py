from date_diff import date_diff_in_Seconds

from datetime import datetime

import sys

sys.path.append("..")


def test_1():
    date1 = datetime.strptime(
        '2024-01-01 01:00:00', '%Y-%m-%d %H:%M:%S'
    )
    date2 = datetime.strptime(
        '2024-01-01 01:00:01', '%Y-%m-%d %H:%M:%S'
    )
    result = date_diff_in_Seconds(date2, date1)
    assert result == 1


def test_60():
    date1 = datetime.strptime(
        '2024-01-01 01:00:00', '%Y-%m-%d %H:%M:%S'
    )
    date2 = datetime.strptime(
        '2024-01-01 01:01:00', '%Y-%m-%d %H:%M:%S'
    )
    result = date_diff_in_Seconds(date2, date1)
    assert result == 60
